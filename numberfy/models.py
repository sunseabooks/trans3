from django.db import models

class Entry(models.Model):
    entries = models.PositiveIntegerField()

class Simplify(models.Model):
    entries = models.ForeignKey(
        "Entry", related_name="simplifys",
        on_delete=models.CASCADE)
