from django.apps import AppConfig


class NumberfyConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "numberfy"
