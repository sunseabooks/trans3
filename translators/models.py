from django.db import models
from .managers import TransManager

# class Entry(models.Model):
#     entries = models.CharField(max_length=15)
#     types = models.ForeignKey(
#         "Type", related_name="entries_type", on_delete=models.CASCADE)

class Language(models.Model):
    name = models.CharField(max_length=15, unique=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    objects = TransManager()

    def __str__(self):
        return f"{str(self.name)}"

class Word(models.Model):
    words = models.CharField(max_length=20, unique=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)
    languages = models.ForeignKey(
        "Language", related_name="words", on_delete=models.CASCADE)

    objects = TransManager()

    def __str__(self):
        return f"{str(self.words)}"


class Type(models.Model):
    types = models.CharField(max_length=20, unique=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    objects = TransManager()

    def __str__(self):
        return f"{str(self.types)}"


class Key(models.Model):
    homes = models.ForeignKey(
        "Word", related_name="keys_home", on_delete=models.CASCADE)
    aways = models.ForeignKey(
        "Word", related_name="keys_away", on_delete=models.CASCADE)
    types = models.ForeignKey(
        "Type", related_name="keys_type", on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    objects = TransManager()

    def __str__(self):
        return f"{str(self.homes)} {str(self.types)} {str(self.aways)}"
