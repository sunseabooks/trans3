# Generated by Django 4.1 on 2022-08-30 15:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Language",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=15, null=True, unique=True)),
                ("date_added", models.DateTimeField(auto_now_add=True)),
                ("date_edited", models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name="Type",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("types", models.CharField(max_length=20, unique=True)),
                ("date_added", models.DateTimeField(auto_now_add=True)),
                ("date_edited", models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name="Word",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("words", models.CharField(max_length=20, unique=True)),
                ("date_added", models.DateTimeField(auto_now_add=True)),
                ("date_edited", models.DateTimeField(auto_now=True)),
                (
                    "languages",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="words",
                        to="translators.language",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Key",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date_added", models.DateTimeField(auto_now_add=True)),
                ("date_edited", models.DateTimeField(auto_now=True)),
                (
                    "aways",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="keys_away",
                        to="translators.word",
                    ),
                ),
                (
                    "homes",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="keys_home",
                        to="translators.word",
                    ),
                ),
                (
                    "types",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="keys_type",
                        to="translators.type",
                    ),
                ),
            ],
        ),
    ]
