from django.urls import path
from .views import language_detail, front_view, search_out, KeyCreate, LanguageList, LanguageCreate, WordCreate


urlpatterns = [
    path("languages/", LanguageList.as_view(), name="list_languages" ),
    path("languages/<int:pk>/", language_detail, name="detail_languages"),
    path("add_languages/", LanguageCreate.as_view(), name="add_languages"),
    path("add_words/", WordCreate.as_view(), name="add_words"),
    path("searched/", search_out, name="search_out"),
    path("", front_view, name="front_view" ),
    path("add_keys/", KeyCreate.as_view(), name="add_keys"),
]
