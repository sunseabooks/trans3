from django.db import models


class TransQuerySet(models.QuerySet):

    def get_keys_words(self, home_value):
        print(f"{home_value}    ANDDREEEEEWWWWWWWWWW")
        return self.filter(homes__words__icontains=home_value)



    def get_keys_list(self, home_value, type_value):
        home_value = 1
        type_value = 1
        return self.filter(
            keys_home=home_value
            ).filter(
                keys_type=type_value,
            )

    def get_latest_list(self, model):
        return self.latest(model)

    # def get_words_test(self, home_value):
    #     return self.filter(homes=home_value)

    def get_words_list(self, language):
        return self.filter(languages=language)
        # return self.filter(Word.languages==languages)


class TransManager(models.Manager):
    def get_queryset(self):
        return TransQuerySet(self.model, using=self._db)

    def get_only_words(self, home_value):
        return self.get_queryset().get_keys_words(home_value)


    # def get_keys(self, home_value, type_value):
    #     return self.get_queryset().get_keys_list(home_value, type_value)

    def get_words(self, language):
        return self.get_queryset().get_words_list(language)

    def get_latest(self, model):
        return self.get_queryset().get_latest_list(model)
