from django.contrib import admin
from .models import Language, Word, Key, Type

admin.site.register(Language)
admin.site.register(Word)
admin.site.register(Key)
admin.site.register(Type)
