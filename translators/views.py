from django.shortcuts import render
from django.views.generic import ListView, DeleteView, DetailView, CreateView, UpdateView
from .models import Language, Word, Key, Type
from django.urls import reverse_lazy


# def show_recipe(request, pk):
#     context = {
#         "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
#         "rating_form": RatingForm(),  # highlight
#     }
#     return render(request, "recipes/detail.html", context)

# def key_list(request, home, type):
#     key_words = Key.objects.get_keys(home, type)
#     context = {
#         "key_words": key_words
#     }
#     return render(request, "translators/entry/out.html", context)

def search_out(request):
    print("hello")
    type_value = ""
    home_value = ""
    keys_list = ""
    if request.method == "POST":
        searched = list(request.POST["searched"])
        length = len(searched)

        for i in range(length):
            if searched[i] in Type.objects.all():
                type_char = searched.pop(i)
                type_value.append(type_char)
        home_value= "".join(searched)


        # x = Key.objects.filter(homes=home_value)
        # print(f"{x}")
        # home_words = Word.objects.get_words_test()



        if type_value == "":
            print(f"{home_value} if type_value == "":")

            # keys = Word.objects.get(words=home_value)
            keys_list = Key.objects.get_only_words(home_value)
        # else:
        #     keys_list = Word.objects.get_keys(home_value, type_value)

    context = {"keys_list": keys_list}
    print(keys_list)
    print("mynameisandreeeeew")
    # print(type(home_value))
    # print(type_value)
    # print(type(type_value))
    return render(request, "translators/search/search_out.html", context)


def language_detail(request, pk):
    language = Language.objects.get(pk=pk)
    # print(f"{type(language)}   language = Language.objects.get(pk=pk) ")
    lang_words = Word.objects.get_words(language)
    context = {
        "lang_words": lang_words
    }
    return render(request, "translators/languages/detail.html", context)


def front_view(request):
    language = Language.objects.get_latest("date_added")
    words = Word.objects.get_latest("date_added")
    types = Type.objects.get_latest("date_added")
    keys = Key.objects.get_latest("date_added")
    # lang_dated = getattr(language, "date_added")
    # words_dated = getattr(words, "date_added")
    # types_dated = getattr(types, "date_added")
    # keys_dated = getattr(keys, "date_added")
    context = {
        "language": language,
        "words": words,
        "types": types,
        "keys": keys,
    }
    return render(request, "translators/front.html", context)

class LanguageList(ListView):
    model = Language
    template_name = "translators/languages/list.html"

class LanguageCreate(CreateView):
    model = Language
    fields = [
        "name",
    ]
    template_name = "translators/languages/create.html"
    success_url = reverse_lazy("add_words")

class KeyCreate(CreateView):
    model = Key
    fields = [
        "homes",
        "aways",
        "types",
    ]
    template_name= "translators/keys/create.html"
    success_url = reverse_lazy("front_view")

class WordCreate(CreateView):
    model = Word
    fields = [
        "languages",
        "words",
    ]
    template_name = "translators/words/create.html"
    success_url = reverse_lazy("list_languages")

# class EntryCreate(CreateView):
#     model = Entry
#     fields = [
#         "entries"
#     ]
#     template_name = "translators/entry/create.html"
#     success_url = reverze_lazy("fdsafd", home, type)
